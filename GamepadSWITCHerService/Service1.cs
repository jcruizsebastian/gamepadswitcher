﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace GamepadSWITCHerService
{
    public partial class Service1 : ServiceBase
    {
        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct ServiceStatus
        {
            public long dwServiceType;
            public ServiceState dwCurrentState;
            public long dwControlsAccepted;
            public long dwWin32ExitCode;
            public long dwServiceSpecificExitCode;
            public long dwCheckPoint;
            public long dwWaitHint;
        };

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);


        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {           
            Thread t = new Thread(new ThreadStart(this.InitTimer));
            t.Start();

        }

        private System.Timers.Timer timer;

        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // TODO: Insert monitoring activities here.
            eventLog1.WriteEntry("Monitoring the System", EventLogEntryType.Information, 5);

            //GamepadSWitcher.Program.Launch();
        }

        protected override void OnStop()
        {
            timer.Enabled = false;

            //GamepadSWitcher.Program.Close();
        }

        private void InitTimer()
        {
            GamepadSWitcher.Program.Launch();

            timer = new System.Timers.Timer();
            //wire up the timer event 
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            //set timer interval   
            //var timeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["TimerIntervalInSeconds"]); 
            double timeInSeconds = 3.0;
            timer.Interval = (timeInSeconds * 1000);
            // timer.Interval is in milliseconds, so times above by 1000 
            timer.Enabled = true;
        }

        protected void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int timer_fired = 0;
        }

        private void eventLog1_EntryWritten(object sender, EntryWrittenEventArgs e)
        {

        }
    }
}
