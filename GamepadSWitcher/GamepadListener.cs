﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GamepadSWitcher
{
    public class GamepadListener
    {
        public enum Button
        {
            X,
            A
        }

        public Button GamepadListenerMethod()
        {
            // Initialize DirectInput
            var directInput = new DirectInput();

            // Find a Joystick Guid
            List<Guid> joystickGuid = new List<Guid>();

            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Gamepad, DeviceEnumerationFlags.AllDevices))
                joystickGuid.Add(deviceInstance.InstanceGuid);

            // If Gamepad not found, look for a Joystick
            if (joystickGuid.Count == 0)
                foreach (var deviceInstance in directInput.GetDevices(DeviceType.Joystick, DeviceEnumerationFlags.AllDevices))
                    joystickGuid.Add(deviceInstance.InstanceGuid);

            // If Joystick not found, throws an error
            if (joystickGuid.Count == 0)
            {
                Console.WriteLine("No joystick/Gamepad found.");
                Console.ReadKey();
                Environment.Exit(1);
            }

            // Instantiate the joystick
            List<Joystick> joysticks = new List<Joystick>();
            
            foreach(Guid guid in joystickGuid)
                joysticks.Add(new Joystick(directInput, guid));

            Console.WriteLine("Found Joystick/Gamepad with GUID: {0}", joystickGuid);

            // Query all suported ForceFeedback effects
            //var allEffects = joystick.GetEffects();
            //foreach (var effectInfo in allEffects)
            //    Console.WriteLine("Effect available {0}", effectInfo.Name);

            foreach (Joystick joystick in joysticks)
            {
                joystick.Properties.BufferSize = 128; // Set BufferSize in order to use buffered data.
                joystick.Acquire(); // Acquire the joystick
            }
            

            // Poll events from joystick
            while (true)
            {
                Thread.Sleep(1000);

                foreach (Joystick joystick in joysticks)
                {
                    joystick.Poll();
                    var datas = joystick.GetBufferedData();

                    foreach (JoystickUpdate state in datas)
                    {
                        Console.WriteLine(state);

                        if (state.Offset == JoystickOffset.Buttons0)
                            return Button.A;

                        if (state.Offset == JoystickOffset.Buttons1)
                            return Button.X;
                    }
                }

            }
        }
    }
}
