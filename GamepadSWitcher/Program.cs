﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GamepadSWitcher
{
    class Program
    {

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        public static void Main()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
            //AppOpener appOpener = new AppOpener();
            //appOpener.OpenSteam();

            //DisplayMode.SetDisplayMode(DisplayMode.DisplayModes.Duplicate);

            //GamepadListener gl = new GamepadListener();
            //gl.GamepadListenerMethod();

            Program program = new Program();
            program.StartingService();
        }

        private void StartingService()
        {
            SetPCScreen();

            AppOpener appOpener = new AppOpener();

            while (true)
            {
                if (!appOpener.IsSomeProcessRuning() && !appOpener.RunningByGamepadSwitcher)
                    StartGamepadListener(appOpener);

                if (!appOpener.IsSomeProcessRuning() && appOpener.RunningByGamepadSwitcher)
                {
                    Thread.Sleep(500);
                    if (appOpener.RunningByGamepadSwitcher)
                    {
                        SetPCScreen();
                        appOpener.RunningByGamepadSwitcher = false;
                    }
                }
            }
        }


        private static void StartGamepadListener(AppOpener appOpener)
        {
            while (true)
            {
                GamepadListener gamepadListener = new GamepadListener();
                GamepadSWitcher.GamepadListener.Button button = gamepadListener.GamepadListenerMethod();

                if (button == GamepadListener.Button.A)
                {
                    SetTVScreen();
                    Thread.Sleep(500);
                    appOpener.OpenSteam();
                    Thread.Sleep(200);
                    BringToFront.bringToFront("steam");
                }

                if (button == GamepadListener.Button.X)
                {
                    SetTVScreen();
                    Thread.Sleep(1000);
                    appOpener.OpenKodi();
                    Thread.Sleep(3000);
                    BringToFront.bringToFront("kodi");
                }

                return;
            }
        }

        private static void SetPCScreen()
        {
            DisplayMode.SetDisplayMode(DisplayMode.DisplayModes.External);
        }

        private static void SetTVScreen()
        {
            DisplayMode.SetDisplayMode(DisplayMode.DisplayModes.Internal);
        }

        public static void Close()
        {
            SetPCScreen();
        }
    }
}
