﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;

namespace GamepadSWitcher
{
    public class AppOpener
    {
        public bool RunningByGamepadSwitcher = false;
        public void OpenSteam()
        {
            Process steamprocess = Process.Start(@"C:\Program Files (x86)\Steam\Steam.exe", "-bigpicture");
            
            RunningByGamepadSwitcher = true;
        }

        public void OpenKodi()
        {
            Process kodiprocess = Process.Start(@"C:\Program Files (x86)\Kodi\Kodi.exe");

            RunningByGamepadSwitcher = true;
        }

        public bool IsSomeProcessRuning()
        {

            if (Process.GetProcessesByName("Steam").Count() > 0 || Process.GetProcessesByName("kodi").Count() > 0)
                return true;

            return false;
        }
    }

}
