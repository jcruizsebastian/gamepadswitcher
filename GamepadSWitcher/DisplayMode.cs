﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GamepadSWitcher
{
    public class DisplayMode
    {

        public static void SetDisplayMode(DisplayModes mode)
        {
            var proc = new Process();

            proc.StartInfo.FileName = "DisplaySwitch.exe";

            switch (mode)
            {
                case DisplayModes.External:
                    proc.StartInfo.Arguments = "/external";
                    break;
                case DisplayModes.Internal:
                    proc.StartInfo.Arguments = "/internal";
                    break;
                case DisplayModes.Extend:
                    proc.StartInfo.Arguments = "/extend";
                    break;
                case DisplayModes.Duplicate:
                    proc.StartInfo.Arguments = "/clone";
                    break;
            }

            proc.Start();
        }

        public enum DisplayModes
        {
            Internal,
            External,
            Extend,
            Duplicate
        }

    }
}
